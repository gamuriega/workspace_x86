/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    printf("presione el numero 1 para abrir la barrera \n");
    printf("presione el numero 2 para cerrar la barrera \n");
    printf("presione ESC para salir del programa \n");

    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_1) {
           
            hw_AbrirBarrera();
        }

        if (input == SENSOR_2) {
            hw_CerrarBarrera();
        }

        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
